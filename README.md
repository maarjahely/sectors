# Sectors

## Running application

```
gradlew bootRun
```

Go to [Swagger](http://localhost:8080/swagger-ui.html) resources

Go to http://localhost:8080/ for application

### Prerequisites

You will need Java 8

## Running the tests

```
gradlew test
```

## Built With

* Java8
* Spring Boot
* Lombok
* SpringFox
* JUnit