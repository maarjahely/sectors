package ee.helmes.sectors.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerWithSectorName {
    private Long id;
    private String customerName;
    private Boolean agreeToTerms;
    private String sector;

}
