package ee.helmes.sectors.web;

import ee.helmes.sectors.dto.CustomerRequest;
import ee.helmes.sectors.dto.CustomerWithSectorName;
import ee.helmes.sectors.entity.CustomersEntity;
import ee.helmes.sectors.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/")
public class CustomerController {
    private final CustomerService customerService;

    @ApiOperation(value = "Add customer to database")
    @PostMapping(value = "customer", consumes = {"application/json"})
    public Long addCustomer(@RequestBody @Valid CustomerRequest customerRequest) {
        return customerService.addCustomer(customerRequest);
    }

    @ApiOperation(value = "Get all customers")
    @GetMapping(value = "customer")
    public List<CustomerWithSectorName> getCustomers() {
        return customerService.getCustomers();
    }

    @ApiOperation(value = "Get customer by id")
    @GetMapping("customer/{customerId}")
    public CustomersEntity getCustomer(@PathVariable Long customerId) {
        return customerService.getCustomer(customerId);
    }

    @ApiOperation(value = "Delete customer by id")
    @DeleteMapping("customer/{customerId}")
    public void deleteCustomer(@PathVariable Long customerId) {
        customerService.deleteCustomer(customerId);
    }

}
