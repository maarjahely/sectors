package ee.helmes.sectors.web;

import ee.helmes.sectors.entity.SectorsEntity;
import ee.helmes.sectors.service.SectorsService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/")
public class SectorsController {
    private final SectorsService sectorsService;

    @ApiOperation(value = "Get all sectors")
    @GetMapping(value = "sectors")
    public List<SectorsEntity> getSectors() {
        return sectorsService.getSectors();
    }
}
