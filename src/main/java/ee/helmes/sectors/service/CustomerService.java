package ee.helmes.sectors.service;

import ee.helmes.sectors.dto.CustomerRequest;
import ee.helmes.sectors.dto.CustomerWithSectorName;
import ee.helmes.sectors.entity.CustomersEntity;
import ee.helmes.sectors.entity.SectorsEntity;
import ee.helmes.sectors.repository.CustomerRepository;
import ee.helmes.sectors.repository.SectorsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@AllArgsConstructor
@Service
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final SectorsService sectorsService;

    public List<CustomerWithSectorName> getCustomers() {
        log.info("Getting all customers");

        Map<Long, String> sectorsIdNameMap = sectorsService.getSectors().stream().collect(
                Collectors.toMap(SectorsEntity::getValue, SectorsEntity::getSectorName));
        return customerRepository.findAll(Sort.by(Sort.Direction.ASC, "id")).stream()
                .map(customersEntity -> mapDataToApi(customersEntity, sectorsIdNameMap))
                .collect(Collectors.toList());

    }

    private CustomerWithSectorName mapDataToApi(CustomersEntity customersEntity, Map<Long, String> sectorsIdNameMap) {

        return CustomerWithSectorName.builder()
                .agreeToTerms(customersEntity.getAgreeToTerms())
                .customerName(customersEntity.getCustomerName())
                .id(customersEntity.getId())
                .sector(sectorsIdNameMap.get(customersEntity.getSector()))
                .build();
    }

    public Long addCustomer(CustomerRequest customerRequest) {
        log.info("Saving customer {}", customerRequest);

        CustomersEntity customersEntity = CustomersEntity.builder()
                .customerName(customerRequest.getCustomerName())
                .agreeToTerms(customerRequest.isAgreeToTerms())
                .sector(customerRequest.getSector())
                .id(customerRequest.getId())
                .build();

        customerRepository.save(customersEntity);
        return customersEntity.getId();
    }

    public CustomersEntity getCustomer(Long customerId) {
        log.info("Getting customer {}", customerId);
        return getCustomerById(customerId);

    }

    public void deleteCustomer(Long customerId) {
        log.info("Deleting customer {}", customerId);
        customerRepository.delete(getCustomerById(customerId));
    }

    private CustomersEntity getCustomerById(Long customerId) {
        return customerRepository.findById(customerId)
                .orElseThrow(() -> new RuntimeException("No customer found with customerId " + customerId));
    }
}
