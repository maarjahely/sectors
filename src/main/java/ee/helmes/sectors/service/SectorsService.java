package ee.helmes.sectors.service;

import ee.helmes.sectors.entity.SectorsEntity;
import ee.helmes.sectors.repository.SectorsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@AllArgsConstructor
@Service
public class SectorsService {
    private final SectorsRepository sectorsRepository;


    @Cacheable("sectors")
    public List<SectorsEntity> getSectors()  {
        log.info("Getting all sectors");
        return sectorsRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

}
