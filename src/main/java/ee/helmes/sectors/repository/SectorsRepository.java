package ee.helmes.sectors.repository;

import ee.helmes.sectors.entity.SectorsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectorsRepository extends JpaRepository<SectorsEntity, Long> {
}