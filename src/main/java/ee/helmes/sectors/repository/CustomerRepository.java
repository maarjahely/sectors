package ee.helmes.sectors.repository;

import ee.helmes.sectors.entity.CustomersEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository  extends JpaRepository<CustomersEntity, Long> {

}
