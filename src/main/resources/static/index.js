$("#sector-form").submit( function (event) {
    event.preventDefault();
    var json = ConvertFormToJSON($(this));
    console.log("Submit ", json);

    $.ajax({
        type: "POST",
        data : JSON.stringify(json),
        contentType: "application/json",
        dataType: "json",
        cache: false,
        url: "http://localhost:8080/customer",
        success: function(data){
            location.reload();
        }
    });

    return true;
});

let dropdown = $('#sector-select');
dropdown.empty();
const sectorsUrl = 'http://localhost:8080/sectors';

// Populate dropdown with list of sectors
$.getJSON(sectorsUrl, function (data) {
  $.each(data, function (key, entry) {
    dropdown.append($('<option></option>').attr('value', entry.value).text(entry.sectorName));
  })
});

const customerUrl = 'http://localhost:8080/customer'

//Populate table with data
$.getJSON(customerUrl, function (data) {
    var table = $('#sectors-table tbody');
    console.log("andmed ", data);
    data.forEach(function (d) {
        var markup = "<tr><td>" + d.id + "</td><td>" + d.customerName + "</td><td>" + d.sector + "</td><td>" +
                         d.agreeToTerms + "</td><td><a href=\"#\" onclick=\"editCustomer(" + d.id +
                         ")\">Edit</a> / <a href=\"#\" onclick=\"deleteCustomer(" + d.id + ")\">Delete</a></td></tr>";
        console.log("markup ", markup);
        table.append(markup);
    });
});

//Add customer data to form
function editCustomer(id) {
    const getCustomerUrl = customerUrl + "/" + id;
    console.log("Muudan kasutajat. Url", getCustomerUrl);
    $.getJSON(getCustomerUrl, function (data) {
        console.log("Andmed urlist ", data);
        $('#customer-name').val(data.customerName);
        $('#sector-select').val(data.sector);
        $('#agree-to-terms').prop('checked', true);
        $('#customer-id').val(data.id);
    });
}

//Clear form
function clearForm() {
    console.log("Tühjendan vormi");
    $('#sector-form')[0].reset();
}

//Delete customer
function deleteCustomer(id) {
    console.log("Kustutan kliendi");
    const deleteCustomerUrl = customerUrl + "/" + id;

    $.ajax({
        url: deleteCustomerUrl,
        type: 'DELETE',
        success: function(result) {
            console.log("Customer deleted");
            location.reload();
        }
    });

}


function ConvertFormToJSON(form){
    var array = jQuery(form).serializeArray();
    var json = {};

    jQuery.each(array, function() {
    if(this.name === 'agreeToTerms') {
        json[this.name] = true;
    } else {
        json[this.name] = this.value || '';
    }
    });

    return json;
}