--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: customers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customers (
    id bigint NOT NULL,
    agree_to_terms boolean,
    customer_name character varying(255),
    sector bigint
);


ALTER TABLE public.customers OWNER TO postgres;

--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customers_id_seq OWNER TO postgres;

--
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customers_id_seq OWNED BY public.customers.id;


--
-- Name: sectors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sectors (
    id bigint NOT NULL,
    sector_name character varying(255),
    value bigint
);


ALTER TABLE public.sectors OWNER TO postgres;

--
-- Name: sectors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sectors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sectors_id_seq OWNER TO postgres;

--
-- Name: sectors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sectors_id_seq OWNED BY public.sectors.id;


--
-- Name: customers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers ALTER COLUMN id SET DEFAULT nextval('public.customers_id_seq'::regclass);


--
-- Name: sectors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sectors ALTER COLUMN id SET DEFAULT nextval('public.sectors_id_seq'::regclass);


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customers (id, agree_to_terms, customer_name, sector) FROM stdin;
2	t	Anna	43
3	t	Peeter	389
4	t	Villem	98
5	t	Maiu	45
6	t	Toomas	75
1	t	Maarja	114
7	t	Jaanus	29
9	t	Sander	39
\.


--
-- Data for Name: sectors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sectors (id, sector_name, value) FROM stdin;
1	Manufacturing	1
2	Construction materials	19
3	Electronics and Optics	18
4	Food and Beverage	6
5	Bakery and confectionery products	342
6	Beverages	43
7	Fish and fish products	42
8	Meat and meat products	40
9	Milk and dairy products	39
10	Other	437
11	Sweets and snack food	378
12	Furniture	13
13	Bathroom/sauna	389
14	Bedroom	385
15	Children’s room	390
16	Kitchen	98
17	Living room	101
18	Office	392
19	Other (Furniture)	394
20	Outdoor	341
21	Project furniture	99
22	Machinery	12
23	Machinery components	94
24	Machinery equipment/tools	91
25	Manufacture of machinery	224
26	Maritime	97
27	Aluminium and steel workboats	271
28	Boat/Yacht building	269
29	Ship repair and conversion	230
30	Metal structures	93
31	Other	508
32	Repair and maintenance service	227
33	Metalworking	11
34	Construction of metal structures	67
35	Houses and buildings	263
36	Metal products	267
37	Metal works	542
38	CNC-machining	75
39	Forgings, Fasteners	62
40	Gas, Plasma, Lasercutting	69
41	MIG, TIG, Aluminum welding	66
42	Plastic and Rubber	9
43	Packaging	54
44	Plastic goods	556
45	Plastic processing technology	559
46	Blowing	55
47	Moulding	57
48	Plastics welding and processing	53
49	Plastic profiles	560
50	Printing	5
51	Advertising	148
52	Book/Periodicals printing	150
53	Labelling and packaging printing	145
54	Textile and Clothing	7
55	Clothing	44
56	Textile	45
57	Wood	8
58	Other (Wood)	337
59	Wooden building materials	51
60	Wooden houses	47
61	Other	3
62	Creative industries	37
63	Energy technology	29
64	Environment	33
65	Service	2
66	Business services	25
67	Engineering	35
68	Information Technology and Telecommunications	28
69	Data processing, Web portals, E-marketing	581
70	Programming, Consultancy	576
71	Software, Hardware	121
72	Telecommunications	122
73	Tourism	22
74	Translation services	141
75	Transport and Logistics	21
76	Air	111
77	Rail	114
78	Road	112
79	Water	113
\.


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customers_id_seq', 9, true);


--
-- Name: sectors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sectors_id_seq', 79, true);


--
-- Name: customers customers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: sectors sectors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sectors
    ADD CONSTRAINT sectors_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

