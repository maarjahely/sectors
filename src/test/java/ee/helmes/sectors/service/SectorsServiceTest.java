package ee.helmes.sectors.service;

import ee.helmes.sectors.entity.SectorsEntity;
import ee.helmes.sectors.repository.SectorsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Sort;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class SectorsServiceTest {

    @Mock
    private SectorsRepository sectorsRepository;

    @Mock
    private SectorsEntity sectorsEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getSectors() {
        when(sectorsRepository.findAll(Sort.by(Sort.Direction.ASC, "id")))
                .thenReturn(Collections.singletonList(sectorsEntity));
        List<SectorsEntity> result = createService().getSectors();
        assertEquals(sectorsEntity,result.get(0));
    }

    private SectorsService createService() {
        return new SectorsService(sectorsRepository);
    }
}