package ee.helmes.sectors.service;

import ee.helmes.sectors.dto.CustomerRequest;
import ee.helmes.sectors.dto.CustomerWithSectorName;
import ee.helmes.sectors.entity.CustomersEntity;
import ee.helmes.sectors.entity.SectorsEntity;
import ee.helmes.sectors.repository.CustomerRepository;
import ee.helmes.sectors.repository.SectorsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Sort;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CustomerServiceTest {
    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private SectorsService sectorsService;

    @Captor
    private ArgumentCaptor<CustomersEntity> customerEntityArgumentCaptor;

    @Mock
    private CustomersEntity customersEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getCustomers() {
        SectorsEntity sectorsEntity = SectorsEntity.builder()
                .value(1L)
                .sectorName("SectorName")
                .build();
        CustomersEntity customers = CustomersEntity.builder()
                .id(1L)
                .sector(1L)
                .agreeToTerms(true)
                .customerName("Eesnimi Perenimi")
                .build();
        when(sectorsService.getSectors())
                .thenReturn(Collections.singletonList(sectorsEntity));
        when(customerRepository.findAll(Sort.by(Sort.Direction.ASC, "id")))
                .thenReturn(Collections.singletonList(customers));

        List<CustomerWithSectorName> result = createService().getCustomers();
        CustomerWithSectorName customerWithSectorName = result.get(0);
        assertEquals(customers.getCustomerName(), customerWithSectorName.getCustomerName());
        assertEquals(customers.getAgreeToTerms(), customerWithSectorName.getAgreeToTerms());
        assertEquals(customers.getId(), customerWithSectorName.getId());
        assertEquals(sectorsEntity.getSectorName(), customerWithSectorName.getSector());

    }

    @Test
    void addCustomer() {
        CustomerRequest customerRequest = CustomerRequest.builder()
                .customerName("Eesnimi Perenimi")
                .sector(1L)
                .agreeToTerms(true)
                .id(1L)
                .build();

        createService().addCustomer(customerRequest);
        verify(customerRepository).save(customerEntityArgumentCaptor.capture());
        CustomersEntity customersEntity = customerEntityArgumentCaptor.getValue();

        assertEquals(customerRequest.getCustomerName(), customersEntity.getCustomerName());
        assertEquals(customerRequest.getSector(), customersEntity.getSector());
        assertEquals(customerRequest.isAgreeToTerms(), customersEntity.getAgreeToTerms());
        assertEquals(customerRequest.getId(), customersEntity.getId());
    }

    @Test
    void getCustomerExists() {
        when(customerRepository.findById(1L)).thenReturn(Optional.of(customersEntity));
        CustomersEntity result = createService().getCustomer(1L);

        assertNotNull(result);
    }

    @Test
    void getCustomerNotExists() {
        when(customerRepository.findById(1L)).thenReturn(Optional.empty());

        RuntimeException assertThrows = assertThrows(RuntimeException.class,
                () -> createService().getCustomer(1L));
        assertEquals("No customer found with customerId 1", assertThrows.getMessage());
    }

    @Test
    void deleteCustomerExists() {
        when(customerRepository.findById(1L)).thenReturn(Optional.of(customersEntity));
        createService().deleteCustomer(1L);
        verify(customerRepository).delete(customersEntity);
    }

    @Test
    void deleteCustomerNotExists() {
        when(customerRepository.findById(1L)).thenReturn(Optional.empty());

        RuntimeException assertThrows = assertThrows(RuntimeException.class,
                () -> createService().deleteCustomer(1L));
        assertEquals("No customer found with customerId 1", assertThrows.getMessage());
    }

    private CustomerService createService() {
        return new CustomerService(customerRepository, sectorsService);
    }
}